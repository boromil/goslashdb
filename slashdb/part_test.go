package slashdb

import "testing"

func TestPart(t *testing.T) {
	testCases := []struct {
		name     string
		part     Part
		expected string
	}{
		{
			name:     "only name",
			part:     Part{Name: "Cat", Separator: ","},
			expected: "/Cat",
		},
		{
			name:     "only fields - one",
			part:     Part{Fields: []string{"name"}, Separator: ","},
			expected: "/name",
		},
		{
			name:     "only fields - multiple",
			part:     Part{Fields: []string{"name", "age", "color"}, Separator: ","},
			expected: "/name,age,color",
		},
		{
			name: "only filters - one",
			part: Part{
				Filter: Filter{
					Values: map[string][]string{"name": {"Jack"}},
					Order:  []string{"name"}},
				Separator: ",",
			},
			expected: "/name/Jack",
		},
		{
			name: "only filters - multiple",
			part: Part{
				Filter: Filter{
					Values: map[string][]string{
						"age":   {"23"},
						"name":  {"Jack"},
						"color": {"red"},
					},
					Order: []string{"name", "age", "color"},
				},
				Separator: ",",
			},
			expected: "/name/Jack/age/23/color/red",
		},
		{
			name:     "name and fields - one",
			part:     Part{Name: "Cat", Fields: []string{"name"}, Separator: ","},
			expected: "/Cat/name",
		},
		{
			name:     "name and fields - multiple",
			part:     Part{Name: "Cat", Fields: []string{"name", "age", "color"}, Separator: ","},
			expected: "/Cat/name,age,color",
		},
		{
			name: "name and filters - one",
			part: Part{
				Name: "Cat",
				Filter: Filter{
					Values: map[string][]string{
						"name": {"Jack"},
					},
					Order: []string{"name"},
				},
				Separator: ",",
			},
			expected: "/Cat/name/Jack",
		},
		{
			name: "name and filters - one field, multiple values",
			part: Part{
				Name: "Cat",
				Filter: Filter{
					Values: map[string][]string{
						"name": {"Jack", "Bob", "Bil"},
					},
					Order: []string{"name"},
				},
				Separator: "$",
			},
			expected: "/Cat/name/Jack$Bob$Bil",
		},
		{
			name: "all - one",
			part: Part{
				Name:   "Cat",
				Fields: []string{"name"},
				Filter: Filter{
					Values: map[string][]string{
						"name": {"Jack"},
					},
					Order: []string{"name"},
				},
				Separator: ",",
			},
			expected: "/Cat/name/Jack/name",
		},
		{
			name: "all - multiple",
			part: Part{
				Name:   "Cat",
				Fields: []string{"age", "city"},
				Filter: Filter{
					Values: map[string][]string{
						"age":   {"23"},
						"name":  {"Jack", "Bob"},
						"color": {"r*", "*d"},
					},
					Order: []string{"name", "age", "color"},
				},
				Separator: ",",
			},
			expected: "/Cat/name/Jack,Bob/age/23/color/r*,*d/age,city",
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(tt *testing.T) {
			out := tc.part.String()
			if out != tc.expected {
				tt.Errorf("expected to get: %q, got: %q\n", tc.expected, out)
			}
		})
	}
}
