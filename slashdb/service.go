package slashdb

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

const (
	idAPIResourceFieldName          = "db_id"
	typeAPIResourceFieldName        = "type"
	descriptionAPIResourceFieldName = "description"
	statusAPIResourceFieldName      = "status"
)

// Resource represens a single SlashDB resource
type Resource struct {
	ID          string `json:"id"`
	Type        string `json:"type"`
	Path        string `json:"path"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

// NewResourceFromMap returns a new resource created from incoming map data
func NewResourceFromMap(data map[string]string, pathKey string) (r Resource, err error) {
	r.ID, err = strValFromMap(data, idAPIResourceFieldName)
	if err != nil {
		return Resource{}, err
	}

	r.Type, err = strValFromMap(data, typeAPIResourceFieldName)
	if err != nil {
		return Resource{}, err
	}

	r.Path, err = strValFromMap(data, pathKey)
	if err != nil {
		return Resource{}, err
	}

	// the description is optional, so we can safely ignore this error
	r.Description, _ = strValFromMap(data, descriptionAPIResourceFieldName)

	r.Status, err = strValFromMap(data, statusAPIResourceFieldName)
	if err != nil {
		return Resource{}, err
	}

	return r, nil
}

// Doer - a simple interface for a http.Client
type Doer interface {
	Do(req *http.Request) (*http.Response, error)
}

// Service - main SlashDB API service
type Service struct {
	host        string
	apiKeyName  string
	apiKeyValue string

	refIDPrefix string
	resources   map[string]Resource

	echoMode bool

	client Doer
}

// NewService - returns a new instance of a SlashDB service
func NewService(
	host, apiKeyName, apiKeyValue, refIDPrefix string,
	echoMode bool,
	httpClient Doer,
) (*Service, error) {
	if _, err := url.Parse(host); err != nil {
		return nil, fmt.Errorf("malformed SlashDB host URL: %w", err)
	}

	return &Service{
		host:        host,
		apiKeyName:  apiKeyName,
		apiKeyValue: apiKeyValue,
		refIDPrefix: refIDPrefix,
		resources:   map[string]Resource{},
		echoMode:    echoMode,
		client:      httpClient,
	}, nil
}

// Init - initializes the service i.e. get the base resource mapping
func (s *Service) Init(ctx context.Context) error {
	var rawResources []map[string]string
	if err := s.Get(ctx, NewDataRequest(""), &rawResources); err != nil {
		return fmt.Errorf("error retriving resources info: %w", err)
	}

	for _, rr := range rawResources {
		r, err := NewResourceFromMap(rr, s.refIDPrefix)
		if err != nil {
			log.Printf("error preparing resource: %v, raw: %v\n", err, rr)
			continue
		}
		s.resources[r.ID] = r
	}

	return nil
}

// Resources - returns a copy of the services resources map
func (s *Service) Resources() map[string]Resource {
	r := make(map[string]Resource, len(s.resources))
	for k, v := range s.resources {
		r[k] = v
	}
	return r
}

func (s *Service) echoRequest(method string, endpoint string, body []byte) {
	if !s.echoMode {
		return
	}
	msg := fmt.Sprint("\ndoing a: ", method, "\nrequest to: ", endpoint)
	if body != nil {
		msg = msg + "\nwith body: " + string(body)
	}
	log.Println(msg)
}

func strValFromMap(m map[string]string, k string) (string, error) {
	v, found := m[k]
	if !found || v == "" {
		return "", fmt.Errorf("%q key not found", k)
	}

	return v, nil
}
