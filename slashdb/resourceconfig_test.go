package slashdb

import (
	"context"
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/boromil/goslashdb/types"
)

const (
	resourceConfigsRespBody = `
	{
		"Northwind": {
			"db_encoding": "iso-8859-2",
			"autoconnect": true,
			"db_id": "Northwind",
			"creator": "admin",
			"read": [],
			"editable": true,
			"db_type": "mssql",
			"executable": true,
			"autoload_user": {
				"dbuser": "admin",
				"dbpass": "admin"
			},
			"autoload": true,
			"viewable": true,
			"write": [],
			"connect_status": "Connected",
			"connection": "some/Northwind",
			"foreign_keys": {},
			"execute": [],
			"db_schema": null,
			"owners": [
				"admin"
			],
			"alternate_key": {
				"Category Sales for 1997": [
					"CategoryName"
				]
			},
			"excluded_columns": {},
			"desc": "Sample MS SQL Database"
		},
		"Chinook": {
			"db_encoding": "utf-8",
			"autoconnect": true,
			"db_id": "Chinook",
			"creator": "admin",
			"read": [],
			"editable": true,
			"db_type": "sqlite",
			"executable": true,
			"autoload_user": {
				"dbuser": "",
				"dbpass": ""
			},
			"autoload": true,
			"viewable": true,
			"write": [],
			"connect_status": "Connected",
			"connection": "chinook.sqlite",
			"foreign_keys": {},
			"execute": [],
			"db_schema": null,
			"owners": [
				"admin"
			],
			"alternate_key": {},
			"excluded_columns": {},
			"desc": "Example SQLite database"
		}
	}`
	resourceConfigRespBody = `
	{
		"db_encoding": "utf-8",
		"autoconnect": true,
		"db_id": "Chinook",
		"creator": "admin",
		"read": [],
		"editable": true,
		"db_type": "sqlite",
		"executable": true,
		"autoload_user": {
			"dbuser": "",
			"dbpass": ""
		},
		"autoload": true,
		"viewable": true,
		"write": [],
		"connect_status": "Connected",
		"connection": "chinook.sqlite",
		"foreign_keys": {},
		"execute": [],
		"db_schema": null,
		"owners": [
			"admin"
		],
		"alternate_key": {},
		"excluded_columns": {},
		"desc": "Example SQLite database"
	}`
	resourceConfigsAccessDeniedRespBody = `{"http_code": 403, "description": "Access was denied to this resource. Please log in with your username/password or resend your request with a valid API key."}`
	resourceConfigNotFoundRespBody      = `{"http_code": 404, "description": "The resource could not be found."}`
)

func TestResourceConfigs(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       resourceConfigsRespBody,
			respStatusCode: http.StatusOK,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	out, err := s.ResourceConfigs(context.Background())
	if err != nil {
		t.Fatalf("%v", err)
	}

	expectedData := map[string]types.ResourceConfig{
		"Northwind": {
			ID:          "Northwind",
			Type:        "mssql",
			Encoding:    "iso-8859-2",
			Desc:        "Sample MS SQL Database",
			Autoload:    true,
			Autoconnect: true,
			Viewable:    true,
			Editable:    true,
			Executable:  true,
			Creator:     "admin",
			Owner:       []string{"admin"},
			Read:        []string{},
			Write:       []string{},
			Execute:     []string{},
			Connection:  "some/Northwind",
			Schema:      "",
			UserCredentials: types.UserCredentials{
				Name:     "admin",
				Password: "admin",
			},
			AlternateKeys: map[string][]string{
				"Category Sales for 1997": {"CategoryName"},
			},
			ExcludedKeys:  map[string][]string{},
			ForeignKeys:   map[string][]string{},
			ConnectStatus: types.Connected,
		},
		"Chinook": {
			ID:              "Chinook",
			Type:            "sqlite",
			Encoding:        "utf-8",
			Desc:            "Example SQLite database",
			Autoload:        true,
			Autoconnect:     true,
			Viewable:        true,
			Editable:        true,
			Executable:      true,
			Creator:         "admin",
			Owner:           []string{"admin"},
			Read:            []string{},
			Write:           []string{},
			Execute:         []string{},
			Connection:      "chinook.sqlite",
			Schema:          "",
			UserCredentials: types.UserCredentials{},
			AlternateKeys:   map[string][]string{},
			ExcludedKeys:    map[string][]string{},
			ForeignKeys:     map[string][]string{},
			ConnectStatus:   types.Connected,
		},
	}

	if len(out) != len(expectedData) {
		t.Fatalf("expected to get: %d resource config object, got: %d", len(expectedData), len(out))
	}

	for k, v := range expectedData {
		uc, found := out[k]
		if !found {
			t.Fatalf("expected to find: %s resource config object", k)
		}
		if !reflect.DeepEqual(uc, v) {
			t.Fatalf("expected to get: %+v resource config object, got: %+v", v, uc)
		}
	}
}

func TestResourceConfigsError(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       resourceConfigsAccessDeniedRespBody,
			respStatusCode: http.StatusForbidden,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	_, err = s.ResourceConfigs(context.Background())
	expectedErrorText := "error retriving resource configs: failed to get request: Access was denied to this resource. Please log in with your username/password or resend your request with a valid API key."
	if err.Error() != expectedErrorText {
		t.Fatalf("expected to get the following error: %q, got %q", expectedErrorText, err.Error())
	}
}

func TestResourceConfig(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       resourceConfigRespBody,
			respStatusCode: http.StatusOK,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	out, err := s.ResourceConfig(context.Background(), "Chinook")
	if err != nil {
		t.Fatalf("%v", err)
	}

	expectedData := types.ResourceConfig{
		ID:              "Chinook",
		Type:            "sqlite",
		Encoding:        "utf-8",
		Desc:            "Example SQLite database",
		Autoload:        true,
		Autoconnect:     true,
		Viewable:        true,
		Editable:        true,
		Executable:      true,
		Creator:         "admin",
		Owner:           []string{"admin"},
		Read:            []string{},
		Write:           []string{},
		Execute:         []string{},
		Connection:      "chinook.sqlite",
		Schema:          "",
		UserCredentials: types.UserCredentials{},
		AlternateKeys:   map[string][]string{},
		ExcludedKeys:    map[string][]string{},
		ForeignKeys:     map[string][]string{},
		ConnectStatus:   types.Connected,
	}

	if !reflect.DeepEqual(out, expectedData) {
		t.Fatalf("expected to get: %+v resource config object, got: %+v", expectedData, out)
	}
}

func TestResourceConfigError(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       resourceConfigNotFoundRespBody,
			respStatusCode: http.StatusNotFound,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	_, err = s.ResourceConfig(context.Background(), "test")
	expectedErrorText := "error retriving resource config: failed to get request: The resource could not be found."
	if err.Error() != expectedErrorText {
		t.Fatalf("expected to get the following error: %q, got %q", expectedErrorText, err.Error())
	}
}
