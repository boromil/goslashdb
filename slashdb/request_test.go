package slashdb

import "testing"

func TestResource(t *testing.T) {
	testCases := []struct {
		name     string
		request  Request
		parts    []Part
		expected string
	}{
		{
			name: "basic test",
			request: Request{
				Kind:      "/db",
				Separator: "%",
				Params: map[string]string{
					"limit": "2",
				},
			},
			parts: []Part{
				{
					Name:   "Cat",
					Fields: []string{"name"},
					Filter: Filter{
						Values: map[string][]string{
							"name": {"Jack", "Bob"},
						},
						Order: []string{"name"},
					},
				},
			},
			expected: "/db/Cat/name/Jack%Bob/name.json?limit=2",
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(tt *testing.T) {
			tc.request.AddParts(tc.parts...)
			out := tc.request.String()
			if out != tc.expected {
				tt.Errorf("expected to get: %q, got: %q\n", tc.expected, out)
			}
		})
	}
}
