package slashdb

import (
	"context"
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/boromil/goslashdb/types"
)

const (
	queryConfigsRespBody = `
	{
		"order-total-by-country": {
			"execute": [
				"test"
			],
			"creator": "admin",
			"read": [
				"test"
			],
			"database": "Chinook",
			"write": [],
			"query_id": "order-total-by-country",
			"http_methods": {
				"GET": true
			},
			"sqlstr": "select BillingCountry as Country, Sum(Total) as Total\nfrom Invoice\ngroup by BillingCountry\norder by 2 desc",
			"desc": "Total orders by country"
		},
		"add-new-customer": {
			"execute": [
				"test",
				"public",
				"admin"
			],
			"creator": "admin",
			"read": [
				"test",
				"public",
				"admin"
			],
			"database": "Chinook",
			"write": [
				"test",
				"admin"
			],
			"query_id": "add-new-customer",
			"http_methods": {
				"POST": true
			},
			"sqlstr": "insert into Customer\n (FirstName, LastName, Phone, City, State, Email)\n values\n (:FirstName, :LastName , :Phone, :City, :State, :Email)",
			"desc": "add a new customer"
		}
	}`
	queryConfigRespBody = `
	{
        "execute": [
            "test",
            "public",
            "admin"
        ],
        "creator": "admin",
        "read": [
            "test",
            "public",
            "admin"
        ],
        "database": "Chinook",
        "write": [
            "test",
            "admin"
        ],
        "query_id": "add-new-customer",
        "http_methods": {
            "POST": true
        },
        "sqlstr": "insert into Customer\n (FirstName, LastName, Phone, City, State, Email)\n values\n (:FirstName, :LastName , :Phone, :City, :State, :Email)",
        "desc": "add a new customer"
    }`
	queryConfigsAccessDeniedRespBody = `{"http_code": 403, "description": "Access was denied to this resource. Please log in with your username/password or resend your request with a valid API key."}`
	queryConfigNotFoundRespBody      = `{"http_code": 404, "description": "The resource could not be found."}`
)

func TestQueryConfigs(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       queryConfigsRespBody,
			respStatusCode: http.StatusOK,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	out, err := s.QueryConfigs(context.Background())
	if err != nil {
		t.Fatalf("%v", err)
	}

	expectedData := map[string]types.QueryConfig{
		"order-total-by-country": {
			ID:         "order-total-by-country",
			DatabaseID: "Chinook",
			Desc:       "Total orders by country",
			SQLStr: `select BillingCountry as Country, Sum(Total) as Total
from Invoice
group by BillingCountry
order by 2 desc`,
			HTTPMethods: map[string]bool{
				"GET": true,
			},
			Creator: "admin",
			Read:    []string{"test"},
			Write:   []string{},
			Execute: []string{"test"},
		},
		"add-new-customer": {
			ID:         "add-new-customer",
			DatabaseID: "Chinook",
			Desc:       "add a new customer",
			SQLStr: `insert into Customer
 (FirstName, LastName, Phone, City, State, Email)
 values
 (:FirstName, :LastName , :Phone, :City, :State, :Email)`,
			HTTPMethods: map[string]bool{
				"POST": true,
			},
			Creator: "admin",
			Read:    []string{"test", "public", "admin"},
			Write:   []string{"test", "admin"},
			Execute: []string{"test", "public", "admin"},
		},
	}

	if len(out) != len(expectedData) {
		t.Fatalf("expected to get: %d query config object, got: %d", len(expectedData), len(out))
	}

	for k, v := range expectedData {
		uc, found := out[k]
		if !found {
			t.Fatalf("expected to find: %s query config object", k)
		}
		if !reflect.DeepEqual(uc, v) {
			t.Fatalf("expected to get: %+v query config object, got: %+v", v, uc)
		}
	}
}

func TestQueryConfigsError(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       queryConfigsAccessDeniedRespBody,
			respStatusCode: http.StatusForbidden,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	_, err = s.QueryConfigs(context.Background())
	expectedErrorText := "error retriving query configs: failed to get request: Access was denied to this resource. Please log in with your username/password or resend your request with a valid API key."
	if err.Error() != expectedErrorText {
		t.Fatalf("expected to get the following error: %q, got %q", expectedErrorText, err.Error())
	}
}

func TestQueryConfig(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       queryConfigRespBody,
			respStatusCode: http.StatusOK,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	out, err := s.QueryConfig(context.Background(), "add-new-customer")
	if err != nil {
		t.Fatalf("%v", err)
	}

	expectedData := types.QueryConfig{
		ID:         "add-new-customer",
		DatabaseID: "Chinook",
		Desc:       "add a new customer",
		SQLStr: `insert into Customer
 (FirstName, LastName, Phone, City, State, Email)
 values
 (:FirstName, :LastName , :Phone, :City, :State, :Email)`,
		HTTPMethods: map[string]bool{
			"POST": true,
		},
		Creator: "admin",
		Read:    []string{"test", "public", "admin"},
		Write:   []string{"test", "admin"},
		Execute: []string{"test", "public", "admin"},
	}

	if !reflect.DeepEqual(out, expectedData) {
		t.Fatalf("expected to get: %+v query config object, got: %+v", expectedData, out)
	}
}

func TestQueryConfigError(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       queryConfigNotFoundRespBody,
			respStatusCode: http.StatusNotFound,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	_, err = s.QueryConfig(context.Background(), "test")
	expectedErrorText := "error retriving query config: failed to get request: The resource could not be found."
	if err.Error() != expectedErrorText {
		t.Fatalf("expected to get the following error: %q, got %q", expectedErrorText, err.Error())
	}
}
