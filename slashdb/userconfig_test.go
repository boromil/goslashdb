package slashdb

import (
	"context"
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/boromil/goslashdb/types"
)

const (
	userConfigsRespBody = `
	{
		"test": {
			"userdef": [],
			"api_key": "some api key",
			"user_id": "test",
			"name": "Test User",
			"creator": "user1",
			"edit": [
				"user1",
				"user2"
			],
			"dbdef": [
				"view"
			],
			"querydef": [
				"view",
				"create"
			],
			"databases": {
				"Chinook": {
					"dbuser": "",
					"dbpass": ""
				},
				"DB2": {
					"dbuser": "db2user",
					"dbpass": "db2pass"
				},
				"DB3": {
					"dbuser": "db3user",
					"dbpass": "db3user"
				}
			},
			"password": "testpass",
			"email": "test@email.com",
			"view": [
				"test"
			]
		},
		"public": {
			"userdef": [],
			"api_key": "",
			"user_id": "public",
			"name": "Public Access",
			"creator": "test",
			"edit": [
				"test"
			],
			"dbdef": [],
			"querydef": [
				"view"
			],
			"databases": {
				"Chinook": {
					"dbuser": "",
					"dbpass": ""
				}
			},
			"password": "passhash",
			"email": "",
			"view": []
		}
	}`
	userConfigRespBody = `
	{
		"userdef": [],
		"api_key": "some api key",
		"user_id": "test",
		"name": "Test User",
		"creator": "user1",
		"edit": [
			"user1",
			"user2"
		],
		"dbdef": [
			"view"
		],
		"querydef": [
			"view",
			"create"
		],
		"databases": {
			"Chinook": {
				"dbuser": "",
				"dbpass": ""
			},
			"DB2": {
				"dbuser": "db2user",
				"dbpass": "db2pass"
			},
			"DB3": {
				"dbuser": "db3user",
				"dbpass": "db3user"
			}
		},
		"password": "testpass",
		"email": "test@email.com",
		"view": [
			"test"
		]
	}`
	userConfigsAccessDeniedRespBody = `{"http_code": 403, "description": "Access was denied to this resource. Please log in with your username/password or resend your request with a valid API key."}`
	userConfigNotFoundRespBody      = `{"http_code": 404, "description": "The resource could not be found."}`
)

func TestUsersConfig(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       userConfigsRespBody,
			respStatusCode: http.StatusOK,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	out, err := s.UserConfigs(context.Background())
	if err != nil {
		t.Fatalf("%v", err)
	}

	expectedData := map[string]types.UserConfig{
		"test": {
			ID:       "test",
			Password: "testpass",
			Name:     "Test User",
			Email:    "test@email.com",
			DSCredentials: map[string]types.UserCredentials{
				"Chinook": {Name: "", Password: ""},
				"DB2":     {Name: "db2user", Password: "db2pass"},
				"DB3":     {Name: "db3user", Password: "db3user"},
			},
			ResourceConfigs: []string{"view"},
			QueryConfigs:    []string{"view", "create"},
			UserConfigs:     []string{},
			Creator:         "user1",
			View:            []string{"test"},
			Edit:            []string{"user1", "user2"},
			APIKey:          "some api key",
		},
		"public": {
			ID:       "public",
			Password: "passhash",
			Name:     "Public Access",
			Email:    "",
			DSCredentials: map[string]types.UserCredentials{
				"Chinook": {Name: "", Password: ""},
			},
			ResourceConfigs: []string{},
			QueryConfigs:    []string{"view"},
			UserConfigs:     []string{},
			Creator:         "test",
			View:            []string{},
			Edit:            []string{"test"},
			APIKey:          "",
		},
	}

	if len(out) != len(expectedData) {
		t.Fatalf("expected to get: %d user config object, got: %d", len(expectedData), len(out))
	}

	for k, v := range expectedData {
		uc, found := out[k]
		if !found {
			t.Fatalf("expected to find: %s user config object", k)
		}
		if !reflect.DeepEqual(uc, v) {
			t.Fatalf("expected to get: %+v user config object, got: %+v", v, uc)
		}
	}
}

func TestUsersConfigError(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       userConfigsAccessDeniedRespBody,
			respStatusCode: http.StatusForbidden,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	_, err = s.UserConfigs(context.Background())
	expectedErrorText := "error retriving user configs: failed to get request: Access was denied to this resource. Please log in with your username/password or resend your request with a valid API key."
	if err.Error() != expectedErrorText {
		t.Fatalf("expected to get the following error: %q, got %q", expectedErrorText, err.Error())
	}
}

func TestUserConfig(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       userConfigRespBody,
			respStatusCode: http.StatusOK,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	out, err := s.UserConfig(context.Background(), "test")
	if err != nil {
		t.Fatalf("%v", err)
	}

	expectedData := types.UserConfig{
		ID:       "test",
		Password: "testpass",
		Name:     "Test User",
		Email:    "test@email.com",
		DSCredentials: map[string]types.UserCredentials{
			"Chinook": {Name: "", Password: ""},
			"DB2":     {Name: "db2user", Password: "db2pass"},
			"DB3":     {Name: "db3user", Password: "db3user"},
		},
		ResourceConfigs: []string{"view"},
		QueryConfigs:    []string{"view", "create"},
		UserConfigs:     []string{},
		Creator:         "user1",
		View:            []string{"test"},
		Edit:            []string{"user1", "user2"},
		APIKey:          "some api key",
	}

	if !reflect.DeepEqual(out, expectedData) {
		t.Fatalf("expected to get: %+v user config object, got: %+v", expectedData, out)
	}
}

func TestUserConfigError(t *testing.T) {
	s, err := NewService(
		"http://demo.slashdb.com",
		"api",
		"key",
		"__href",
		false,
		&testDoer{
			respBody:       userConfigNotFoundRespBody,
			respStatusCode: http.StatusNotFound,
		},
	)
	if err != nil {
		t.Fatalf("%v", err)
	}

	_, err = s.UserConfig(context.Background(), "test")
	expectedErrorText := "error retriving user config: failed to get request: The resource could not be found."
	if err.Error() != expectedErrorText {
		t.Fatalf("expected to get the following error: %q, got %q", expectedErrorText, err.Error())
	}
}
