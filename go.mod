module gitlab.com/boromil/goslashdb

go 1.13

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/stretchr/testify v1.4.0 // indirect
)
