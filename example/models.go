package main

// Album - represents an album data model
type Album struct {
	AlbumID int    `json:"AlbumId" xml:"AlbumId"`
	Title   string `json:"Title" xml:"Title"`
}

// Artist - represents an artist data model
type Artist struct {
	ArtistID int     `json:"ArtistId" xml:"ArtistId"`
	Name     string  `json:"Name" xml:"Name"`
	Albums   []Album `json:"Album,omitempty" xml:"Album,omitempty"`
}

// CustomerSubset - represents an subset of the customer data model
type CustomerSubset struct {
	FirstName string `json:"FirstName" xml:"FirstName"`
	LastName  string `json:"LastName" xml:"LastName"`
	City      string `json:"City" xml:"City"`
	State     string `json:"State,omitempty" xml:"State,omitempty"`
}
