package main

// Config holds the main configuration attributes
type Config struct {
	Production bool `env:"PRODUCTION" envDefault:"false"`

	SlashDBHost           string `env:"SLASHDB_HOST" envDefault:"https://demo.slashdb.com"`
	SlashDBAPIKeyName     string `env:"SLASHDB_API_KEY_NAME" envDefault:"apikey"`
	SlashDBAPIKeyValue    string `env:"SLASHDB_API_KEY_VALUE" envDefault:"AuEKt464x8p9X50J8k83SMk3poaU2rsc"`
	SlashDBAPIRefIDPrefix string `env:"SLASHDB_REF_ID_PREFIX" envDefault:"__href"`
}
