package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/caarlos0/env"
	"gitlab.com/boromil/goslashdb/slashdb"
)

func main() {
	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatalf("failed to parse configs: %v", err)
	}

	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	defer cancelAppCtx()

	sdbHTTPClient := &http.Client{
		Timeout: 10 * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: !cfg.Production,
			},
			TLSHandshakeTimeout:   3 * time.Second,
			ResponseHeaderTimeout: 3 * time.Second,
			ExpectContinueTimeout: 3 * time.Second,
			IdleConnTimeout:       10 * time.Second,
			MaxIdleConns:          100,
		},
	}

	sdbService, err := slashdb.NewService(
		cfg.SlashDBHost,
		cfg.SlashDBAPIKeyName,
		cfg.SlashDBAPIKeyValue,
		cfg.SlashDBAPIRefIDPrefix,
		!cfg.Production,
		sdbHTTPClient,
	)
	if err != nil {
		log.Fatalf("failed to create SlashDB service: %v", err)
	}

	if err = sdbService.Init(appCtx); err != nil {
		log.Fatalf("failed init SlashDB: %v", err)
	}
	for id, resource := range sdbService.Resources() {
		log.Printf("%q: %+v\n", id, resource)
	}

	if _, err = sdbService.ResourceConfigs(appCtx); err != nil {
		// if our user can't access the config data, we'll get an (hopefully helpful) error
		log.Printf("%v\n", err)
	}

	// create the base part
	chinook := slashdb.Part{Name: "Chinook"}

	// lets create a new request object
	artistReq := slashdb.NewDataRequest("")
	// add some 'prats' to it
	artistReq.AddParts(
		chinook,
		slashdb.Part{Name: "Artist"},
	)
	// lets get 10 Artist
	artistReq.SetLimit(10)
	// using the 'depth' param lets also get their album
	artistReq.SetDepth(2)

	artists := make([]Artist, 0, 10)
	// now lets get the request data
	if err = sdbService.Get(appCtx, artistReq, &artists); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("%v\n", artists)
	}

	// let get only the names for the 10 artist
	artistNamesReq := slashdb.NewDataRequest("")
	artistNamesReq.AddParts(
		chinook,
		slashdb.Part{
			Name:   "Artist",
			Fields: []string{"Name"},
		},
	)
	artistNamesReq.SetLimit(10)

	artistNames := make([]string, 0, 10)
	if err = sdbService.Get(appCtx, artistNamesReq, &artistNames); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("%v\n", artistNames)
	}

	// lets get all artists with names
	// beginning with "B" and ending in "n"
	bArtistReq := slashdb.NewDataRequest("")
	// add some 'parts' to it
	bArtistReq.AddParts(
		chinook,
		slashdb.Part{
			Name: "Artist",
			Filter: slashdb.NewFilter(
				map[string][]string{"Name": {"B*n"}},
				nil,
			),
		},
	)
	bArtistReq.SetLimit(10)
	bArtistReq.SetDepth(2)

	bArtists := make([]Artist, 0, 10)
	if err = sdbService.Get(appCtx, bArtistReq, &bArtists); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("%v\n", bArtists)
	}

	// we can get more than one field in on request
	// just for fun, lest filter the clients a bit
	// and use something else than "*" as the wildcard
	customerSubsetReq := slashdb.NewDataRequest("")
	customerSubsetReq.AddParts(
		chinook,
		slashdb.Part{
			Name: "Customer",
			// take all customers where "FirstName" start with "M"
			// and "LastName" ends in "s"
			Filter: slashdb.NewFilter(
				map[string][]string{
					"LastName":  {"$s"},
					"FirstName": {"M$"},
				},
				[]string{"FirstName", "LastName"},
			),
			// take 3 specified fields from the filtered data
			Fields: []string{"City", "FirstName", "LastName"},
		},
	)
	customerSubsetReq.SetLimit(10)
	customerSubsetReq.SetWildcard("$")

	filteredCustomers := []CustomerSubset{}
	if err = sdbService.Get(appCtx, customerSubsetReq, &filteredCustomers); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("%v\n", filteredCustomers)
	}

	customerSubsetSortedReq := slashdb.NewDataRequest("")
	customerSubsetSortedReq.AddParts(
		chinook,
		slashdb.Part{
			Name: "Customer",
			Filter: slashdb.NewFilter(
				map[string][]string{
					"LastName":  {"$s"},
					"FirstName": {"M$"},
				},
				[]string{"FirstName", "LastName"},
			),
			Fields: []string{"City", "FirstName", "LastName"},
		},
	)
	customerSubsetSortedReq.SetLimit(10)
	customerSubsetSortedReq.SetWildcard("$")
	// sort the data by "LastName" and then "City"
	customerSubsetSortedReq.SetSort(
		slashdb.Desc("LastName"), slashdb.Desc("City"),
	)

	filteredSortedCustomers := []CustomerSubset{}
	if err = sdbService.Get(appCtx, customerSubsetSortedReq, &filteredSortedCustomers); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("%v\n", filteredSortedCustomers)
	}

	// create an new artist
	newArtistReq := slashdb.NewDataRequest("")
	newArtistReq.AddParts(chinook, slashdb.Part{Name: "Artist"})

	newArtist := struct{ Name string }{Name: "Just A Test"}
	resp, err := sdbService.Create(appCtx, newArtistReq, newArtist)
	if err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("created object %+v\n", resp)
	}

	// and update the name
	updatedArtistReq := slashdb.NewDataRequest("")
	updatedArtistReq.AddParts(
		chinook,
		slashdb.Part{
			Name: "Artist",
			Filter: slashdb.NewFilter(
				map[string][]string{
					"ArtistId": {resp.ID},
				},
				nil,
			),
		},
	)

	updatedArtist := struct{ Name string }{Name: "Updated Artist Name"}
	if err := sdbService.Update(appCtx, updatedArtistReq, updatedArtist); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("updated object %s\n", updatedArtistReq)
	}

	// finally delete the created/updated object,
	// we can just resuse the update request object
	if err := sdbService.Delete(appCtx, updatedArtistReq); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("deleted object %s\n", updatedArtistReq)
	}

	// making SlashDBs SQL Pass-thru queries
	// works the same as for other data sources,
	// we just use a different constructor function
	queryReq := slashdb.NewQueryRequest("")
	queryReq.AddParts(
		slashdb.Part{
			Name: "customers-in-city",
			Filter: slashdb.NewFilter(
				map[string][]string{
					"city": {"New York"},
				},
				nil,
			),
		},
	)

	queryCustomers := make([]CustomerSubset, 0, 1)
	if err := sdbService.Get(appCtx, queryReq, &queryCustomers); err != nil {
		log.Printf("%v\n", err)
	} else {
		fmt.Printf("%v\n", queryCustomers)
	}
}
