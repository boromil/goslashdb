# Golang library interfacing with [SlashDBs](https://slashdb.com) [API](https://docs.slashdb.com/user-guide/)

This library is compatible with SlashDB ver. 1.1.0.

For use, see the example folder. The GoDoc can be found [here](https://godoc.org/gitlab.com/boromil/goslashdb).